package torsten.ansorg;

/**
 * Created by Torsten Ansorg <ta@plazz.ag>
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;


public class frame extends JFrame implements ActionListener {

    public frame (Dimension dim, String title, LayoutManager layout) {
        this.setTitle(title);
        this.setLayout(layout);
        this.setIcon();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setFrameSize(dim);
        this.setLocationRelativeTo(null);
    }

    public void setIcon () {
        try {
            URL iconURL = getClass().getResource("resources/icon.png");
            ImageIcon icon = new ImageIcon(iconURL);
            this.setIconImage(icon.getImage());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void setFrameSize(Dimension dim) {
        this.setSize(dim);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        // add some Action Listener here
    }
}
