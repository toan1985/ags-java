package torsten.ansorg;


import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;

class tempData {
    private ArrayList<Object> data;

    public tempData(ArrayList<Object> d) {
        this.data = d;
    }

    public ArrayList<Object> getData() {
        return this.data;
    }
}



public class MenuActionListener implements ActionListener {

    private int identifier;
    private static MySQLConnection mysql = null;
    private JFrame frame = null;
    private JTable tab = null;

    //static sql-settings
    private static String dbName = "db_temp";
    private static String dbUser = "root";
    private static String dbPass = "";

    public JTable getTab() {
        return this.tab;
    }

    private static void getMysqlInstance() {
        try {
            mysql = new MySQLConnection(dbName, dbUser, dbPass);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private void exitAction (){
        System.exit(0);
    }

    private boolean getTempAction() {

        if(mysql == null) {
            getMysqlInstance();
        }

        ArrayList<String> expressions = new ArrayList<String>();
        expressions.add("ORDER BY mess_id ASC");
        // expressions.add("LIMIT 5");
        ResultSet res = mysql.fetchRows("tb_temp_messungen", expressions);
        int c = 0;
        try {
            ResultSetMetaData resMd = res.getMetaData();
            c = resMd.getColumnCount();
        }
        catch (Exception e) {
            System.out.print(e.getStackTrace());
        }

        DefaultTableModel model = new DefaultTableModel();
        Object[] colNames = new Object[c];
        ArrayList<tempData> td = new ArrayList<tempData>();

        try {
            for(int i = 1; i <= c; i++) {
                colNames[i-1] = res.getMetaData().getColumnName(i);
            }

            while(res.next()) {
                ArrayList<Object> dat = new ArrayList<Object>();
                for(int i = 1; i <= res.getMetaData().getColumnCount(); i++) {
                    dat.add(res.getObject(i));
                }
                tempData d = new tempData(dat);
                td.add(d);
            }

            Object[][] dT = new Object[td.size()][];
            int i = 0;
            for(tempData next : td) {
                dT[i++] = next.getData().toArray();
            }

            this.tab = new JTable(dT,colNames);


            this.tab.setModel(model);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public MenuActionListener(int ident, JFrame frame) {
        this.identifier = ident;
        this.frame = frame;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        switch(this.identifier) {
            case 0 :    this.exitAction();
                        break;
            case 1 :    if(this.getTempAction()) {
                            frame.setTitle("Datenbank abgerufen");
                            JScrollPane scrollPane = new JScrollPane(this.tab);
                            scrollPane.setViewportView(this.tab);
                            frame.add(scrollPane, BorderLayout.CENTER);
                            SwingUtilities.updateComponentTreeUI(frame);
                        }
                        break;
            default:    break;
        }

    }

}