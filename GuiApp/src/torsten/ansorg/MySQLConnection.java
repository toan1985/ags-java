package torsten.ansorg;

/**
 * Created by Torsten Ansorg <ta@plazz.ag>
 */

import java.sql.*;
import java.util.ArrayList;

public class MySQLConnection {

    private Connection conn = null;

    private String dbHost = "localhost";
    private String dbPort = "3306";
    private String db = "";

    private String dbUser = "root";
    private String dbPass = "mysql";

    public MySQLConnection() {
        this.initConnection();
    }

    /**
     * @param db
     * @param user
     * @param pass
     */
    public MySQLConnection(String db, String user, String pass) {
        this.db = db;
        this.dbUser = user;
        this.dbPass = pass;
        this.initConnection();
    }


    // getter/setter

    /**
     * @param host
     */
    public void setHost(String host) {
        this.dbHost = host;
        this.conn = null;
    }

    /**
     * @param port
     */
    public void setPort(String port) {
        this.dbPort = port;
        this.conn = null;
    }

    /**
     * @param db
     */
    public void setDBName(String db) {
        this.db = db;
        this.conn = null;
    }

    /**
     * @return Hostname
     */
    public String getHost() {
        return this.dbHost;
    }

    /**
     * @return Port
     */
    public String getPort() {
        return this.dbPort;
    }

    /**
     * @return Database-Name
     */
    public String getDBName() {
        return this.db;
    }


    private void initConnection(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            this.conn = DriverManager.getConnection("jdbc:mysql://" + this.dbHost + ":" + this.dbPort
                                                                    + "/" + this.db + "?"
                                                                    + "user=" + this.dbUser + "&"
                                                                    + "password=" + this.dbPass);
        } catch (ClassNotFoundException e) {
            System.out.println("driver not found");
        } catch (SQLException e) {
            System.out.println("unable to connect");
        } catch (Exception e) {
            throw e;
        }
    }

    private Connection getInstance(){
        if(this.conn == null)
            this.initConnection();
        return this.conn;
    }

    /**
     *  returns Resultset SQL-Query fetched
     * @param table
     * @return ResultSet
     */
    public ResultSet fetchRows(String table, ArrayList<String> xpressions) {
        ResultSet result = null;
        this.conn = this.getInstance();
        if(this.conn != null) {
            Statement qry;
            String sql = "SELECT * FROM "+table;
            for(String xpression: xpressions) {
                sql += " "+xpression;
            }
            System.out.println(sql);
            try {
                qry = this.conn.createStatement();
                result = qry.executeQuery(sql);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * preprepare SQL-String <-- lol
     * @param sql
     * @param vals
     * @return sql
     */
    private String addSQLVals (String sql, ArrayList<ArrayList<Object>> vals) {
        String h = "";
        if(vals.size() > 0) {
            sql += " (";
            h += " Values(";
            int i = 1;
            for (ArrayList<Object> row : vals) {
                sql += row.get(0).toString();
                h += "?";
                if(i < vals.size() ) {
                    sql += ", ";
                    h += ", ";
                }
                i++;
            }
            sql += ") ";
            h += ")";
        }
        sql += h;
        return sql;
    }

    /**
     * inserts Record into table
     * @param table
     * @param vals
     * @return
     */
    public boolean insertRecord(String table, ArrayList<ArrayList<Object>> vals) {
        this.conn = this.getInstance();
        if(this.conn != null) {
            String sql = "INSERT INTO "+ table;
            sql = this.addSQLVals(sql, vals);

            try {
                PreparedStatement stmnt = this.conn.prepareStatement(sql);
                int i = 1;
                for(ArrayList<Object> row: vals) {
                    //feel free to add additional field types
                    if(row.get(1) instanceof Integer) {
                        stmnt.setInt(i, (int)row.get(1));
                    } else if (row.get(1) instanceof Boolean) {
                        stmnt.setBoolean(i, (boolean) row.get(1));
                    } else {
                        stmnt.setString(i, row.get(1).toString());
                    }
                    i++;
                }
                stmnt.executeUpdate();
                return true;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

}


