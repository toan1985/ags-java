package torsten.ansorg;

/**
 * Created by Torsten Ansorg <ta@plazz.ag>
 */

import javax.swing.*;
import java.awt.*;
import java.net.URL;

public class menu extends JMenu {

    private int style = Font.BOLD;
    private String ff = "Comic Sans MS";

    private String mTitle = "";
    private ImageIcon mIcon = null;

    private void initMenu() {
        this.setText(this.mTitle);
        Font f = new Font (this.ff, this.style , 13);
        this.setFont(f);
        if(this.mIcon != null){
            this.setIcon(this.mIcon);
        }
    }

    public menu (String title, String IconPath) {
        this.mTitle = title;
        try {
            URL iconURL = getClass().getResource(IconPath);
            this.mIcon = new ImageIcon(iconURL);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        this.initMenu();
    }

    public menu (String title) {
        this.mTitle=title;
        this.initMenu();
    }
}
