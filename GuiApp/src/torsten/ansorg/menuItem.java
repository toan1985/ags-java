package torsten.ansorg;

/**
 * Created by Torsten Ansorg <ta@plazz.ag>
 */

import javax.swing.*;
import java.net.URL;


public class menuItem extends JMenuItem {

    private String itemText = "";
    private String iconPath = "";
    private ImageIcon itemIcon = null;
    private JMenuItem item = null;

    public menuItem() {
        initMenuItem();
    }

    /**
     * @param text
     */
    public menuItem(String text) {
        this.itemText = text;
        initMenuItem();
    }

    /**
     * @param text
     * @param iconPath
     */
    public menuItem(String text, String iconPath) {
        this.itemText = text;
        this.iconPath = iconPath;
        initMenuItem();
    }

    private void initMenuItem () {
        if(this.item == null) {
            if(this.getInstance()) {

                this.setText(this.itemText);

                if(this.iconPath != "") {
                    try {
                        URL iconURL = getClass().getResource(iconPath);
                        this.itemIcon = new ImageIcon(iconURL);
                        this.setIcon(this.itemIcon);
                    } catch (Exception e) {
                        System.out.println(e.getMessage());
                    }
                }
            } else {
                System.out.println("couldn't create an Instance of JMenuItem");
            }
        } else {
            if(this.itemText != "") {
                this.setText(this.itemText);
            }
            if(this.iconPath != "") {
                try {
                    URL iconURL = getClass().getResource(iconPath);
                    this.itemIcon = new ImageIcon(iconURL);
                    this.setIcon(this.itemIcon);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    private boolean getInstance() {
        try {
            this.item = new JMenuItem();
            return true;
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
            // throw e;
        }
        return false;
    }


    public void add2Parent(JMenu menu) {
        try {
            menu.add(this);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void add2Parent(menuItem menu) {
        try {
            menu.add(this);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
