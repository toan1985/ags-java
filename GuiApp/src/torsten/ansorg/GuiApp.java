package torsten.ansorg;

/**
 * Created by Torsten Ansorg <ta@plazz.ag>
 */

import javax.swing.*;
import java.awt.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


public class GuiApp {

    //frame-settings
    private int width = 600;
    private int height = 400;
    private String title = "TA GUI App";
    private LayoutManager layout = new BorderLayout();

    //static vars
    private static JFrame app = null;
    private static JMenuBar bar = null;

    /**
     * Main method just starts the App
     * @param args
     */
    public static void main(String[] args) {
        runApp();
    }

    public static void runApp() {
        if (app == null) {
            new GuiApp();
        }
        buildMenuEntries();
        app.setJMenuBar(bar);
        app.setVisible(true);
    }

    private GuiApp() {
        try {
            app = new frame(new Dimension(this.width, this.height), this.title, this.layout);
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    private static void initMenuBar() {
        try {
            bar = new JMenuBar();
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }


    private static menu getMenu (String t, String ic) {
        menu m = new menu(t, ic);
        return m;
    }
    private static menu getMenu (String t) {
        menu m = new menu(t);
        return m;
    }

    private static void buildMenuEntries() {
        if (bar == null) {
            initMenuBar();
        }
        // Home-Menu
        menu homeMenu = getMenu("Home", "resources/icon_home.png");
        menu sqlMenu = getMenu("SQL-Temp", "resources/icon_sql.png");
        menuItem gTemp = new menuItem("Temperatur abrufen");
        menuItem exitItem = new menuItem("Close", "resources/icon_close.png");


        //action listeners
        exitItem.addActionListener(new MenuActionListener(0, app));
        gTemp.addActionListener(new MenuActionListener(1, app));

        gTemp.add2Parent(sqlMenu);
        homeMenu.add(sqlMenu);
        homeMenu.addSeparator();
        homeMenu.add(exitItem);

        bar.add(homeMenu);
    }
}